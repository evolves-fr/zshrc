//
//  zshrcApp.swift
//  zshrc
//
//  Created by Thomas Fournier on 06/10/2020.
//

import SwiftUI

@main
struct zshrcApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
